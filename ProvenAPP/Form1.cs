﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProvenAPP
{
    public partial class Form1 : Form
    {
        UsersDAO usersDAO;
        Users userCorrecto;

        public Form1()
        {           
            InitializeComponent();
            usersDAO = new UsersDAO();        
        }

        private void btn_aceptar_Click(object sender, EventArgs e)
        {
            //recogemos el usuario y la contraseña introducida.
            string user = tb_username.Text;
            string password = tb_password.Text;
            
            //creo el usuario que intenta acceder.
            Users userIntento = new Users(user, password);
            
            //recibo el usuario que intento validar si es correcto lo recibiré sino sera null.
            userCorrecto = usersDAO.validate(userIntento);


            if (userCorrecto != null)
            {
                switch (userCorrecto.Rol)
                {
                    case "admin":
                        this.Hide();
                        MessageBox.Show("Validacio Correcta!  (ADMIN)","LOGIN");
                        Administrar f2 = new Administrar(userCorrecto);
                        f2.ShowDialog();//formulario modal: primer plano
                        break;

                    case "teacher":
                        this.Hide();
                        MessageBox.Show("Validació Correcta! (TEACHER)","LOGIN");
                        Teacher f3 = new Teacher(userCorrecto);
                        f3.ShowDialog();
                        break;

                    default:
                        MessageBox.Show("Ha ocurrido un error inesperado");
                        break;
                }
                
            }
            else
            {
                tb_username.Text = "";
                tb_password.Text = "";
                MessageBox.Show("VALIDACIO INCORRECTA!", "LOGIN ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tb_username.Focus();
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            var result = MessageBox.Show("Seguro que quieres salir!", "SALIR", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
           
            
        }
    }
}
