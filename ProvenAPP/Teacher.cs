﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProvenAPP
{
    public partial class Teacher : Form
    {
        AulaDAO aulaDAO = new AulaDAO();
        FranjasDAO franjasDAO = new FranjasDAO();
        ReservaDAO reservaDAO = new ReservaDAO();
        List<RadioButton> listaRadioButtons = new List<RadioButton>();
        List<Aula> lista_aulas;
        List<Franjas> lista_franjas;
        List<Reserva> lista_reservas;
        DateTime dateTime;
        Aula aulaEscogida = new Aula();
        Franjas franjaAulaEscogida = new Franjas();
        string diaDeLaSemana;
        string id_aulaEscogida;
        int contador_reservas;
        Users user;


        public Teacher(Users userCorrecto)
        {
            InitializeComponent();
            user = userCorrecto;
            
        }

        
        
        private void Teacher_Load(object sender, EventArgs e)
        {            
            lista_aulas = aulaDAO.cargaDatosAulas();
            lista_franjas = franjasDAO.cargaDatosFranjas();
            contador_reservas = reservaDAO.cantidadDeReservas();

        }


        private void sortirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.ShowDialog();
            
        }

        private void llistarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            cargaRadioButtonsDinamicos();
        }
        private void btnListar_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            cargaRadioButtonsDinamicos();
        }

        private void cargaRadioButtonsDinamicos()
        {
            if (lista_aulas == null)
            {
                MessageBox.Show("No arribo al fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int i = 0;            

                foreach (Aula aula in lista_aulas)//recorrem la llista
                {

                    Label lb = new Label();
                    lb.Text = "- "+aula.Nombre.ToUpper()+":";
                    lb.FlatStyle = FlatStyle.Flat;
                    lb.ForeColor = Color.Black;
                    
                    lb.Location = new System.Drawing.Point(50, 109 + i * 45);
                    lb.Size = new System.Drawing.Size(100, 35);
                    

                    Controls.Add(lb);


                    RadioButton rb = new RadioButton();
                    rb.Text = aula.AulaID;
                    rb.FlatStyle = FlatStyle.Flat;
                    rb.ForeColor = Color.Black;
                    rb.BackColor = Color.White;
                    rb.FlatAppearance.BorderColor = Color.Black;

                    rb.Location = new System.Drawing.Point(170, 100 + i * 45);
                    rb.Size = new System.Drawing.Size(65, 36);
                    rb.Click += new System.EventHandler(radioButtonAction);
                    Controls.Add(rb);
                    i++;
                    listaRadioButtons.Add(rb);
                    
                }
                
            }
        }

        private void radioButtonAction(object sender, EventArgs e)
        {

            RadioButton rb = (RadioButton)sender;
            
            string textRadiob;
            textRadiob = rb.Text;
            textBox2.Visible = true;
            textBox2.Text = "";
            

            for (int i = 0; i < lista_aulas.Count; i++)
            {
                if (textRadiob.Equals(lista_aulas[i].AulaID))
                {
                    textBox2.Text = lista_aulas[i].Descripcion + " \r\n\r\nDISPONIBILIDAD:  " + lista_franjas[i].DiaSemana.ToUpper() + "   " + lista_franjas[i].FranjaHora;

                }

            }

        }

        private void btnReservar_Click(object sender, EventArgs e)
        {
            textBox2.Visible = false;
            groupBox1.Visible = true;
            comboBox1.Items.Clear();
            comboBox1.Text = lista_aulas[0].AulaID;
            for (int i = 0; i < lista_aulas.Count; i++)
            {

                comboBox1.Items.Add(lista_aulas[i].AulaID);
            }


        }
        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox2.Visible = false;
            groupBox1.Visible = true;
            comboBox1.Items.Clear();
            comboBox1.Text = lista_aulas[0].AulaID;
            for (int i = 0; i < lista_aulas.Count; i++)
            {

                comboBox1.Items.Add(lista_aulas[i].AulaID);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            id_aulaEscogida = comboBox1.SelectedItem.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reservarAula();
                        
        }

        private void reservarAula()
        {
            try
            {
                bool IsReservada = false;
                dateTime = dateTimePicker1.Value;
                string fechaSeleccionadaReserva = dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year;

                diaDeLaSemana = dateTime.DayOfWeek.ToString();

                if (id_aulaEscogida == null || dateTime == null)
                {
                    MessageBox.Show("No has seleccionado correctamente." ,"SELECCIONA UNA FECHA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(fechaSeleccionadaReserva + " " + diaDeLaSemana, "FECHA", MessageBoxButtons.OK);
                    for (int i = 0; i < lista_aulas.Count; i++)
                    {
                        if (id_aulaEscogida.Equals(lista_aulas[i].AulaID.ToString()))
                        {
                            aulaEscogida = new Aula(lista_aulas[i].AulaID, lista_aulas[i].Nombre, lista_aulas[i].Descripcion);
                            franjaAulaEscogida = new Franjas(lista_franjas[i].FranjaId, lista_franjas[i].DiaSemana, lista_franjas[i].FranjaHora);

                        }
                    }
                    if (diaDeLaSemana.ToLower().Equals(franjaAulaEscogida.DiaSemana.ToLower()))
                    {
                        //deberia leer y comprobar que en el fichero de reservas no hay ninguna ese dia

                        lista_reservas = reservaDAO.lecturaDeReservas();


                        for (int i = 0; i < lista_reservas.Count; i++)
                        {

                            if (fechaSeleccionadaReserva.Equals(lista_reservas[i].FechaReserva))
                            {
                                MessageBox.Show("Fecha NO disponible, Ya está reservada por el/la profesor "+ lista_reservas[i].NombreProfesor.ToUpper(), "FECHA NO DISPONIBLE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                IsReservada = true;
                                break;
                            }
                        }
                        if (IsReservada == false)
                        {
                            bool añadidaReserva = false;
                            contador_reservas++;
                            Reserva reserva = new Reserva("idr" + contador_reservas, fechaSeleccionadaReserva, aulaEscogida.Nombre,user.Username,user.Email);
                            añadidaReserva = reservaDAO.AddReserva(reserva);
                            MessageBox.Show("Reserva Satisfactoria", "RESERVA", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Este aula NO se puede reservar este dia." , "FECHA NO DISPONIBLE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SALTA EL CATCH");
            }
        }
  
    }
}
