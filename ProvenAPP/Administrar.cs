﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProvenAPP
{
    public partial class Administrar : Form
    {
        List<Users> listaUsuarios;
        List<Users> listaTeachers;
        UsersDAO usersDAO;
        Users userToRemove;
        Users userConectado;
        string id;
        string nomUsuari;
        string password;
        string rol;
        string email;
        int contadorPersonas;

        public Administrar(Users userCorrecto)
        {
            InitializeComponent();
            userConectado = userCorrecto;
            usersDAO = new UsersDAO();
            listaUsuarios = usersDAO.mostrarListaUsers();
            listaTeachers = usersDAO.getListaTeachers();
        }

        private void Administrar_Load(object sender, EventArgs e)
        {

        }

        private void Administrar_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            Form1 f1 = new Form1();
            f1.Show();
            

        }

        private void altaUsuarisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox5.Visible = false;
            //Esto solo es para colocar en el textbox el id de la persona siguiente que vamos a añadir.
            contadorPersonas = listaUsuarios.Count + 1;
            tb_id.Text = contadorPersonas.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result = 0;

            //recogemos los datos introducidos para añadir el nuevo usuario(teacher).
            id = tb_id.Text.ToLower();
            nomUsuari = tb_nomUsuari.Text.ToLower();
            password = tb_password.Text.ToLower();
            rol = tb_rol.Text.ToLower();
            email = tb_email.Text.ToLower();

            //si dejamos alguna de las casillas vacias 
            if (id.Length <= 0 || nomUsuari.Length <= 0 || password.Length <= 0 || rol.Length <= 0 || email.Length <= 0)
            {
                MessageBox.Show("Datos incompletos");
                contadorPersonas--;
            }
            else
            {

                Users newUserIntento = new Users(id, nomUsuari, password, rol, email);
                if (newUserIntento != null)
                {
                    result = usersDAO.addUser(newUserIntento);
                    
                }
            }

            switch (result)
            {
                case 1:
                    tb_id.Text = (listaUsuarios.Count + 1).ToString();
                    int ultimoUserAdd = listaUsuarios.Count - 1;
                    var nameUser = listaUsuarios[ultimoUserAdd].Username;
                    MessageBox.Show("Usuario " + nameUser + " añadido correctamente.");
                    tb_nomUsuari.Text = "";
                    tb_password.Text = "";
                    tb_email.Text = "";
                    break;
                case -1:
                    MessageBox.Show("Nombre usuario demasiado largo.");
                    contadorPersonas = listaUsuarios.Count - 1;
                    break;
                case -2:
                    MessageBox.Show("La contraseña debe tener minimo 6 caracteres");
                    contadorPersonas = listaUsuarios.Count - 1;
                    break;
                case -3:
                    MessageBox.Show("El rol no es teacher");
                    contadorPersonas = listaUsuarios.Count - 1;
                    break;
                case -4:
                    MessageBox.Show("El mail no es valido");
                    contadorPersonas = listaUsuarios.Count - 1;
                    break;
                default:
                    MessageBox.Show("Usuario no fue añadido correctamente.");
                    contadorPersonas = listaUsuarios.Count - 1;
                    break;
            }


        }
        //metodo LISTAR teachers
        private void llistarTotsElUsuarisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //listo solo todos los usuarios que sean teachers

            groupBox1.Visible = false;
            groupBox2.Visible = true;
            groupBox3.Visible = false;
            groupBox5.Visible = false;

            listView1.Items.Clear();

            if (listaUsuarios == null)
            {
                MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                for (int i = 0; i < listaUsuarios.Count; i++)
                {
                    if (listaUsuarios[i].Rol.Equals("teacher"))
                    {
                        ListViewItem item = new ListViewItem();
                        item = listView1.Items.Add(listaUsuarios[i].Id);
                        item.SubItems.Add(listaUsuarios[i].Username);
                        item.SubItems.Add(listaUsuarios[i].Password);
                        item.SubItems.Add(listaUsuarios[i].Rol);
                        item.SubItems.Add(listaUsuarios[i].Email);
                    }
                    else
                    {

                    }

                }

            }
        }

        //mostramos el listview de MODIFICACIO.
        private void modificacióToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = true;
            groupBox5.Visible = false;

            listView2.Items.Clear();

            if (listaUsuarios == null)
            {
                MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                for (int i = 0; i < listaUsuarios.Count; i++)
                {
                    if (listaUsuarios[i].Rol.Equals("teacher"))
                    {
                        ListViewItem item = new ListViewItem();
                        item = listView2.Items.Add(listaUsuarios[i].Id);
                        item.SubItems.Add(listaUsuarios[i].Username);
                        item.SubItems.Add(listaUsuarios[i].Password);
                        item.SubItems.Add(listaUsuarios[i].Rol);
                        item.SubItems.Add(listaUsuarios[i].Email);

                    }
                    else
                    {

                    }

                }

            }

        }

        //muestra para el metodo de MODIFICACIO el groupbox xon los botones de cambio de contraseña y mail.
        private void listView2_MouseClick(object sender, MouseEventArgs e)
        {
            groupBox4.Visible = true;
            var userIndex = listView2.SelectedItems[0].SubItems[0].Text;

            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].Id.Equals(userIndex))
                {

                    userToRemove = new Users(listaUsuarios[i].Id, listaUsuarios[i].Username, listaUsuarios[i].Password, listaUsuarios[i].Rol, listaUsuarios[i].Email);
                    tbCambioContraseña.Text = userToRemove.Password.ToString();
                    tbCambioMail.Text = userToRemove.Email.ToString();

                }

            }

        }
        //metodo de MODIFICACIO de contraseña o mail.
        private void btnCambiar_Click(object sender, EventArgs e)
        {
            if (tbCambioContraseña.Text.Length <= 0 || tbCambioMail.Text.Length <= 0)
            {
                MessageBox.Show("Los campos estan vacios, no se pudo cambiar.");
            }
            else
            {
                string newPassword = tbCambioContraseña.Text;
                string newMail = tbCambioMail.Text;

                if (newPassword.Length >= 6 && new EmailAddressAttribute().IsValid(newMail))
                {
                    Users userChange = new Users(userToRemove.Id, userToRemove.Username, newPassword, userToRemove.Rol, newMail);

                    List<Users> listaActualizada = usersDAO.cambiarDatosTeacher(userToRemove, newPassword, newMail);

                    listView2.Items.Clear();
                    if (listaUsuarios == null)
                    {
                        MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {

                        for (int i = 0; i < listaActualizada.Count; i++)
                        {
                            if (listaActualizada[i].Rol.Equals("teacher"))
                            {
                                ListViewItem item = new ListViewItem();
                                item = listView2.Items.Add(listaActualizada[i].Id);
                                item.SubItems.Add(listaActualizada[i].Username);
                                item.SubItems.Add(listaActualizada[i].Password);
                                item.SubItems.Add(listaActualizada[i].Rol);
                                item.SubItems.Add(listaActualizada[i].Email);
                            }
                            else
                            {

                            }
                        }
                    }

                    groupBox4.Visible = false;
                    MessageBox.Show("SE ACTUALIZARON LOS DATOS");

                }
                else
                {
                    MessageBox.Show("NO SE ACTUALIZO");
                }

            }
        }

        //metodo no utilizado finalmente.
        private void cargaListView2()
        {
            listView2.Items.Clear();
            if (listaUsuarios == null)
            {
                MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                for (int i = 0; i < listaTeachers.Count; i++)
                {

                    ListViewItem item = new ListViewItem();
                    item = listView2.Items.Add(listaTeachers[i].Id);
                    item.SubItems.Add(listaTeachers[i].Username);
                    item.SubItems.Add(listaTeachers[i].Password);
                    item.SubItems.Add(listaTeachers[i].Rol);
                    item.SubItems.Add(listaTeachers[i].Email);

                }

            }
        }


        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //metodo mostrar interfaz groupbox de ELIMINACIO
        private void eliminacióToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox5.Visible = true;

            listView3.Items.Clear();

            if (listaUsuarios == null)
            {
                MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                for (int i = 0; i < listaUsuarios.Count; i++)
                {
                    if (listaUsuarios[i].Rol.Equals("teacher"))
                    {
                        ListViewItem item = new ListViewItem();
                        item = listView3.Items.Add(listaUsuarios[i].Id);
                        item.SubItems.Add(listaUsuarios[i].Username);
                        item.SubItems.Add(listaUsuarios[i].Password);
                        item.SubItems.Add(listaUsuarios[i].Rol);
                        item.SubItems.Add(listaUsuarios[i].Email);
                    }
                    else
                    {

                    }

                }

            }
        }



        //metodo ELIMINACIO
        //
        //for (int i = 0; i < listaUsuarios.Count; i++)
        //{
        //    if (listaUsuarios[i].Id.Equals(userIndice))
        //    {

        //        userToRemove = new Users(listaUsuarios[i].Id, listaUsuarios[i].Username, listaUsuarios[i].Password, listaUsuarios[i].Rol, listaUsuarios[i].Email);

        //        List<Users> listaActualizada = usersDAO.removeUserTeacher(userToRemove);                    

        //        if (listaUsuarios == null)
        //        {
        //            MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //        else
        //        {

        //            for (int j = 0; j < listaActualizada.Count; j++)
        //            {
        //                if (listaActualizada[j].Rol.Equals("teacher"))
        //                {
        //                    ListViewItem item = new ListViewItem();
        //                    item = listView3.Items.Add(listaActualizada[j].Id);
        //                    item.SubItems.Add(listaActualizada[j].Username);
        //                    item.SubItems.Add(listaActualizada[j].Password);
        //                    item.SubItems.Add(listaActualizada[j].Rol);
        //                    item.SubItems.Add(listaActualizada[j].Email);
        //                }
        //                else
        //                {

        //                }

        //            }

        //        }
        //    }
        //}




        //metodo ELIMINACIO doubleclik
        private void listView3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var result = MessageBox.Show("SEGURO QUE DESEAS ELIMINAR", "ELIMINAR", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (result == DialogResult.No)
            {

            }
            else
            {
                //var userIndex = listView3.SelectedItems[0].SubItems[0].Text;
                var userIndice = listView3.SelectedIndices[0];


                listView3.Items.Clear();


                listaUsuarios = usersDAO.removeUserTeacher2(userIndice);
                if (listaUsuarios == null)
                {
                    MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    for (int j = 0; j < listaUsuarios.Count; j++)
                    {
                        if (listaUsuarios[j].Rol.Equals("teacher"))
                        {
                            ListViewItem item = new ListViewItem();
                            item = listView3.Items.Add(listaUsuarios[j].Id);
                            item.SubItems.Add(listaUsuarios[j].Username);
                            item.SubItems.Add(listaUsuarios[j].Password);
                            item.SubItems.Add(listaUsuarios[j].Rol);
                            item.SubItems.Add(listaUsuarios[j].Email);
                        }
                        else
                        {

                        }
                    }
                }
            }
        }
    }
}


