﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Reserva
    {
        string idReserva;
        string fechaReserva;
        string aulaReservada;
        string nombreProfesor;
        string emailProfesor;

        public string IdReserva { get => idReserva; set => idReserva = value; }
        public string FechaReserva { get => fechaReserva; set => fechaReserva = value; }
        public string AulaReservada { get => aulaReservada; set => aulaReservada = value; }
        public string NombreProfesor { get => nombreProfesor; set => nombreProfesor = value; }
        public string EmailProfesor { get => emailProfesor; set => emailProfesor = value; }

        public Reserva(string idReserva, string fechaReserva, string aulaReservada, string nombreProfesor, string emailProfesor)
        {
            this.idReserva = idReserva;
            this.fechaReserva = fechaReserva;
            this.aulaReservada = aulaReservada;
            this.nombreProfesor = nombreProfesor;
            this.emailProfesor = emailProfesor;
        }

        public Reserva(string idReserva, string fechaReserva, string aulaReservada)
        {
            this.idReserva = idReserva;
            this.fechaReserva = fechaReserva;
            this.aulaReservada = aulaReservada;
        }
    }
}
