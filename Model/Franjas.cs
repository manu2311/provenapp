﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Franjas
    {
        private string franjaId;
        private string diaSemana;
        private string franjaHora;

        public string FranjaId { get => franjaId; set => franjaId = value; }
        public string DiaSemana { get => diaSemana; set => diaSemana = value; }
        public string FranjaHora { get => franjaHora; set => franjaHora = value; }
        

        public Franjas(string franjaId, string diaSemana, string franjaHora)
        {
            this.franjaId = franjaId;
            this.diaSemana = diaSemana;
            this.franjaHora = franjaHora;
        }

        public Franjas()
        {
        }
    }
}
