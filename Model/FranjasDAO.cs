﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class FranjasDAO
    {
        List<Franjas> lista_franjas = new List<Franjas>();

        public List<Franjas> cargaDatosFranjas()
        {

            string linea;
            string[] split;

            try
            {
                using (StreamReader sr = new StreamReader("franjas.txt"))
                {
                    while ((linea = sr.ReadLine()) != null)
                    {
                        split = linea.Split(';');

                        lista_franjas.Add(new Franjas(split[0], split[1], split[2]));
                    }
                }
            }
            catch (IOException ioe)
            {
            }



            return lista_franjas;
        }




    }
}
