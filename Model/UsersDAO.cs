﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UsersDAO
    {
        List<Users> lista_users = new List<Users>();
        List<Users> lista_modificada;
        

        public UsersDAO()
        {
            lista_users = cargaDatosEnLista();
        }

        public List<Users> cargaDatosEnLista()
        {
            string linea;
            string[] split;

            try
            {
                using (StreamReader sr = new StreamReader("users.txt"))
                {
                    while ((linea = sr.ReadLine()) != null)
                    {
                        split = linea.Split(':');

                        lista_users.Add(new Users(split[0], split[1], split[2], split[3], split[4]));
                    }
                }
            }
            catch (IOException ioe)
            {
            }
            return lista_users;
        }

        


        public Users validate(Users userIntento)
        {
            
            Users userCorrecto = null;
            
            foreach (var user in lista_users)
            {
                if (user.Username.Equals(userIntento.Username) && user.Password.Equals(userIntento.Password))
                {
                    userCorrecto = new Users(user.Id, user.Username, user.Password, user.Rol, user.Email);
                    break;
                }

            }
            return userCorrecto;
        }

        // VALIDACION PARA AÑADIR EN LISTA
        public int addUser(Users newUserIntento)
        {
            bool validMail = false;

            try
            {
                if (newUserIntento.Username.Length >= 10)
                {
                    return -1;
                }
                if (newUserIntento.Password.Length <= 5)
                {
                    return -2;
                }
                if (!newUserIntento.Rol.Equals("teacher"))
                {
                    return -3;
                }
                if (new EmailAddressAttribute().IsValid(newUserIntento.Email))
                {
                    validMail = true;
                }
                else
                {
                    validMail = false;
                    return -4;
                }

                lista_users.Add(newUserIntento);
                saveUserInFile(newUserIntento);



            }
            catch (Exception e)
            {

            }

            return 1;
        }

        //AÑADIR EN PERSISTENCIA FICHERO
        private void saveUserInFile(Users newUser)
        {
            try
            {
                StreamWriter sw = new StreamWriter("users.txt", true);
                sw.WriteLine(newUser.Id + ":" + newUser.Username + ":" + newUser.Password + ":" + newUser.Rol + ":" + newUser.Email + ":");
                sw.Close();
            }catch(Exception ex)
            {
                ex.ToString();
            }
        }

        public List<Users> mostrarListaUsers()
        {
            return lista_users;
        }
        public List<Users> getListaTeachers()
        {
            List<Users> listaTeachers = new List<Users>();
            for (int i = 0; i < lista_users.Count; i++)
            {
                if (lista_users[i].Rol.Equals("teacher"))
                {
                    Users newUser2 = new Users(lista_users[i].Id, lista_users[i].Username, lista_users[i].Password, lista_users[i].Rol, lista_users[i].Email);
                    listaTeachers.Add(newUser2);

                }
            }
            return listaTeachers;


        }
        //MODIFY EN LISTA
        public List<Users> cambiarDatosTeacher(Users userCambioContraseñaMail, string newPassword, string newMail)
        {
            
            Users userChange = new Users(userCambioContraseñaMail.Id, userCambioContraseñaMail.Username, userCambioContraseñaMail.Password, userCambioContraseñaMail.Rol, userCambioContraseñaMail.Email);
            

            Users userChange2 = new Users(userCambioContraseñaMail.Id, userCambioContraseñaMail.Username, newPassword, userCambioContraseñaMail.Rol, newMail);

            int w= (Convert.ToInt32(userCambioContraseñaMail.Id) - 1);

            lista_users.RemoveAt(w);
            lista_users.Insert(w, userChange2);

            lista_modificada = lista_users;

            cargaDatosEnListaModificados(lista_modificada);

            return lista_modificada;
        }

        //MODIFY Y REMOVE PERSISTENCIA FICHERO
        private void cargaDatosEnListaModificados(List<Users> lista_modificada)
        {
            StreamWriter sw = new StreamWriter("users.txt");
            for(int i=0; i < lista_modificada.Count; i++)
            {
                sw.WriteLine(lista_modificada[i].Id + ":" + lista_modificada[i].Username + ":" + lista_modificada[i].Password +":"+ lista_modificada[i].Rol +":"+ lista_modificada[i].Email + ":");
            }
            
            sw.Close();
        }

        public List<Users> removeUserTeacher(Users userToRemove)
        {
            Users userDelete = new Users(userToRemove.Id, userToRemove.Username, userToRemove.Password, userToRemove.Rol, userToRemove.Email);

            int w = (Convert.ToInt32(userToRemove.Id)-1);
            lista_users.RemoveAt(w);

            lista_modificada = lista_users;

            cargaDatosEnListaModificados(lista_modificada);

            return lista_modificada;
        }

        public List<Users> removeUserTeacher2(int userIndice)
        {
            lista_users.RemoveAt(userIndice+2);
            lista_modificada = lista_users;

            cargaDatosEnListaModificados(lista_modificada);

            return lista_modificada;

        }
    }
}
