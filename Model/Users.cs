﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Users
    {
        private string id;
        private string username;
        private string password;
        private string rol;
        private string email;

        public string Id { get => id; set => id = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Rol { get => rol; set => rol = value; }
        public string Email { get => email; set => email = value; }

        public Users(string username, string password, string email) : this(username, password)
        {
            this.username = username;
            this.password = password;
            this.email = email;
        }

        public Users(string id, string username, string password, string rol, string email)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.rol = rol;
            this.email = email;
        }

        public Users(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }


}
