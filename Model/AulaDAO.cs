﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{

    public class AulaDAO
    {
        List<Aula> lista_aulas = new List<Aula>();



        public List<Aula> cargaDatosAulas()
        {

            string linea;
            string[] split;

            try
            {
                using (StreamReader sr = new StreamReader("aulas.txt"))
                {
                    while ((linea = sr.ReadLine()) != null)
                    {
                        split = linea.Split(':');

                        lista_aulas.Add(new Aula(split[0], split[1], split[2]));
                    }
                }
            }
            catch (IOException ioe)
            {
            }



            return lista_aulas;
        }

        
        
    }
}





