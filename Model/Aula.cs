﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Aula
    {
        private string aulaID;
        private string nombre;
        private string descripcion;

        public string AulaID { get => aulaID; set => aulaID = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }

        public Aula(string aulaID, string nombre, string descripcion)
        {
            this.aulaID = aulaID;
            this.nombre = nombre;
            this.descripcion = descripcion;
        }

        public Aula()
        {
        }
    }
}
