﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ReservaDAO
    {
        List<Reserva> lista_reservas = new List<Reserva>();
        Reserva reserva;


        public List<Reserva> lecturaDeReservas()
        {
            string linea;
            string[] split;

            try
            {
                using (StreamReader sr = new StreamReader("reservas.txt"))
                {
                    while ((linea = sr.ReadLine()) != null)
                    {
                        split = linea.Split(':');

                        lista_reservas.Add(new Reserva(split[0], split[1], split[2], split[3], split[4]));
                    }
                }
            }
            catch (IOException ioe)
            {
            }


            return lista_reservas;
        }

        public bool AddReserva(Reserva reserva)
        {
            try
            {               
                StreamWriter sw = new StreamWriter("reservas.txt", true);
                sw.WriteLine(reserva.IdReserva+ ":" + reserva.FechaReserva + ":" + reserva.AulaReservada + ":" + reserva.NombreProfesor + ":" + reserva.EmailProfesor + ":");
                sw.Close();

                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();

                return false;
            }
        }

        public int cantidadDeReservas()
        {
            string linea;
            string[] split;
            int cantidadDeReservas=0;
            List<Reserva> lista_reserva = new List<Reserva>();

            try
            {
                using (StreamReader sr = new StreamReader("reservas.txt"))
                {
                    while ((linea = sr.ReadLine()) != null)
                    {
                        split = linea.Split(':');

                        lista_reserva.Add(new Reserva(split[0], split[1], split[2], split[3], split[4]));
                    }
                    cantidadDeReservas = lista_reserva.Count();
                }
            }
            catch (IOException ioe)
            {
            }

            return cantidadDeReservas;
        }
       
    }
}
